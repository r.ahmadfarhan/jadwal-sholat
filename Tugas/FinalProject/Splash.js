import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image
} from 'react-native';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.circle} />
        <View style={styles.logo}>
            <Image source={require('./images/logo-splash.png')} style={{ width:129, height: 160 }} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems : 'center',
    justifyContent: 'center',
    backgroundColor: '#E5E5E5'
  },
  circle: {
    borderRadius: 200,
    width: 276,
    height: 276,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    alignItems: 'center',
    position: 'absolute',
    height: 160, 
  },
});