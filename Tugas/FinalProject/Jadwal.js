import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
} from 'react-native';

export default class Jadwal extends Component {
    render() {
        let jadwal = this.props.jadwal;
        return (
            <View style={styles.container}>
                <View style={styles.title}>
                    <View style={styles.logo}>
                        <Image source={require('./images/logo.png')} style={{ width:50, height: 50, marginLeft:17, marginRight:24 }} />
                    </View>
                    <View style={styles.subtitle}>
                        <View style={styles.desc}>
                            <Image source={require('./assets/jadwal-sholat.png')} style={{width:114, height:14}} />
                            <Text style={styles.descText}>{jadwal.date.gregorian}</Text>
                        </View>
                        <View style={styles.location}>
                            <Image source={require('./assets/location.png')} style={{width:256, height:26}} />
                        </View>
                    </View>
                </View>
                <View style={styles.separator} />
                <View style={styles.body}>
                    <View style={styles.box}>
                        <View style={styles.icon}>
                            <Image source={require('./images/masjid-icon.png')} style={{width:66, height:55}} />
                        </View>
                        <View style={styles.jadwal}>
                            <Text style={styles.sholat}>Shubuh</Text>
                            <Text style={styles.waktu}>{jadwal.times.Fajr}</Text>
                        </View>
                    </View>
                    <View style={styles.box}>
                        <View style={styles.icon}>
                            <Image source={require('./images/masjid-icon.png')} style={{width:66, height:55}} />
                        </View>
                        <View style={styles.jadwal}>
                            <Text style={styles.sholat}>Dhuhur</Text>
                            <Text style={styles.waktu}>{jadwal.times.Dhuhr}</Text>
                        </View>
                    </View>
                    <View style={styles.box}>
                        <View style={styles.icon}>
                            <Image source={require('./images/masjid-icon.png')} style={{width:66, height:55}} />
                        </View>
                        <View style={styles.jadwal}>
                            <Text style={styles.sholat}>Ashar</Text>
                            <Text style={styles.waktu}>{jadwal.times.Asr}</Text>
                        </View>
                    </View>
                    <View style={styles.box}>
                        <View style={styles.icon}>
                            <Image source={require('./images/masjid-icon.png')} style={{width:66, height:55}} />
                        </View>
                        <View style={styles.jadwal}>
                            <Text style={styles.sholat}>Maghrib</Text>
                            <Text style={styles.waktu}>{jadwal.times.Maghrib}</Text>
                        </View>
                    </View>
                    <View style={styles.box}>
                        <View style={styles.icon}>
                            <Image source={require('./images/masjid-icon.png')} style={{width:66, height:55}} />
                        </View>
                        <View style={styles.jadwal}>
                            <Text style={styles.sholat}>Isya</Text>
                            <Text style={styles.waktu}>{jadwal.times.Isha}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 0
    },
    title: {
        flexDirection: 'row',
        marginHorizontal: 14,
        marginVertical: 4
      },
      logo: {
        alignItems: 'center',
      },
      subtitle: {
        flex: 1
      },
      desc: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 7
      },
      descText: {
        fontFamily: "Comfortaa", 
        fontSize: 16, 
      },
      location: {
        alignItems: 'center'  
      },
      locationText: {
        textAlign: 'center',
        fontSize: 24,
        fontWeight: 'bold',
        color: '#3EC6FF'
      },
      separator: {
        borderColor: '#000000',
        borderBottomWidth: 3,
        marginVertical: 4,
        marginHorizontal:14
      },
      body: {
        padding: 0,
        marginHorizontal: 22
      },
    box: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#66D2FF',
        borderRadius: 8,
        marginTop: 12,
        paddingTop: 12,
        paddingHorizontal: 17,
        shadowColor: "#000",
        shadowOffset: {width: 1, height: 3,},
        shadowOpacity: 0.3,
        shadowRadius: 6,
        elevation: 10,
      },
      icon: {
        justifyContent: 'center',
        alignItems: 'center'
      },
      jadwal: {
        width: 230,
        marginLeft: 35
      },
      sholat:{
        fontSize: 24,
        fontWeight: 'bold',
        lineHeight: 26
      },
      waktu: {
        fontSize: 48,
        fontWeight: 'bold',
        lineHeight: 56,
        textAlign: 'right',
      }
});