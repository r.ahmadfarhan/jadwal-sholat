import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    TextInput
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { AuthContext } from "./context";

const Register = ({ navigation }) => {
    const { signUp } = React.useContext(AuthContext);
    return (
      <View style={styles.container}>
        <View style={styles.union}>
            <Image source={require('./assets/Union.png')} style={{ width:13, height: 11 }} />
        </View>
        <View style={styles.register}>
            <Image source={require('./assets/Register.png')} style={{ width:162, height: 40 }} />
            <Image source={require('./images/logo.png')} style={{ width:70, height: 70 }} />
        </View>
        <View style={styles.form}>
            <TextInput style={{ height: 52, borderWidth: 2, marginBottom: 16 }} placeholder='Name' placeholderTextColor='#747474' /> 
            <TextInput style={{ height: 52, borderWidth: 2, marginBottom: 16 }} placeholder='Email' placeholderTextColor='#747474' /> 
            <TextInput style={{ height: 52, borderWidth: 2, marginBottom: 16 }} placeholder='Password' placeholderTextColor='#747474' /> 
            <TextInput style={{ height: 52, borderWidth: 2, marginBottom: 16 }} placeholder='Confirm Password' placeholderTextColor='#747474' /> 
        </View>
        <TouchableOpacity style={styles.button} onPress={() => signUp()}>
            <Text style={{fontFamily: 'Roboto', fontSize: 13, color: '#FFFFFF', fontWeight: 900}}>REGISTER</Text>
        </TouchableOpacity>
        <View style={styles.footer}>
            <Image source={require('./images/masjid.png')} style={{ height: 230 }} />
        </View>
      </View>
    );
}
export default Register;

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#FFFFFF'
    },
    union: {
    marginLeft: 18,
    marginTop: 30
  },
  register: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 18,
    marginVertical: 17,
    height: 70, 
  },
  form: {
    marginHorizontal: 18
  },
  button: {
    height: 52,
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 18,
    marginVertical: 16,
    backgroundColor: '#000000'
  },
  footer: {
    flex: 1,
    justifyContent: 'flex-end'
  }
});