import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    TextInput,
    FlatList,
    ActivityIndicator
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Jadwal from './Jadwal';

const Separator = () => (
    <View style={styles.separator} />
);

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true
    };
  }

  componentDidMount() {
    fetch('https://api.pray.zone/v2/times/today.json?city=jakarta')
      .then((response) => response.json())
      .then((json) => {
        this.setState({ data: json.results });
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  }

  render() {
    const { data, isLoading } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.header}>
            <View style={styles.menu}>
                <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
                    <Icon name="menu" size={50} style={{width:35}} />
                </TouchableOpacity>
            </View>
            <View style={styles.search}>
                <TextInput style={{ height: 50, borderWidth: 1, borderRadius: 10, width: 277 }} placeholder='Search' placeholderTextColor='#747474' /> 
            </View>
            <View style={styles.magnify}>
                <Icon name="magnify" size={30} />
            </View>
        </View>
        <Separator />
        <View style={{flex:1}}>
        {isLoading ? <ActivityIndicator/> : (
            <FlatList
                data={data.datetime}
                renderItem={(jadwal)=><Jadwal jadwal={jadwal.item} />}
                keyExtractor={(item)=>item.id}
                ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
            />
        )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  header: {
    marginHorizontal: 22,
    marginTop: 30,
    marginBottom: 2,
    flexDirection: 'row',
    height: 52
  },
  menu: {
    height: 50,
    marginRight: 14,
  },
  search: {
    alignItems: 'center',
    marginRight: 5
  },
  magnify: { 
    justifyContent: 'center',
  },
  title: {
    flexDirection: 'row',
    marginHorizontal: 14,
    marginVertical: 4
  },
  logo: {
    alignItems: 'center',
  },
  subtitle: {
    flex: 1
  },
  desc: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 7
  },
  descText: {
    fontFamily: "Comfortaa", 
    fontSize: 16, 
  },
  location: {
    alignItems: 'center'  
  },
  locationText: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: 'bold',
    color: '#3EC6FF'
  },
  separator: {
    borderColor: '#000000',
    borderBottomWidth: 3,
    marginVertical: 4,
    marginHorizontal:14
  },
  body: {
    padding: 0,
    marginHorizontal: 22
  },
  box: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#66D2FF',
    borderRadius: 8,
    marginTop: 12,
    paddingTop: 12,
    paddingHorizontal: 17,
    shadowColor: "#000",
    shadowOffset: {width: 1, height: 3,},
    shadowOpacity: 0.3,
    shadowRadius: 6,
    elevation: 10,
  },
  icon: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  jadwal: {
    width: 230,
    marginLeft: 35
  },
  sholat:{
    fontSize: 24,
    fontWeight: 'bold',
    lineHeight: 26
  },
  waktu: {
    fontSize: 48,
    fontWeight: 'bold',
    lineHeight: 56,
    textAlign: 'right',
  }
});