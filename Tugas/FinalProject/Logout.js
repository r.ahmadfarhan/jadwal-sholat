import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.wallpaper}>
            <Image source={require('./images/wallpaper.jpg')} style={{ width:411, height: 647, position: 'absolute' }} />
            <Image source={require('./images/logo-logout.png')} style={{ width:411, height: 108, marginTop:94 }} />
        </View>
        <View style={styles.button}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                <Image source={require('./assets/login-button.png')} style={{ width:167, height: 52 }} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
                <Image source={require('./assets/register-button.png')} style={{ width:167, height: 52 }} />
            </TouchableOpacity>
        </View>
        <View style={styles.sparator} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#FFFFFF'
  },
  wallpaper: {
    height: 647,
    marginHorizontal: 0,
    alignItems: 'center',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 34,
    marginVertical: 10,
  },
  sparator: {
    borderBottomColor: '#000000',
    borderBottomWidth: 5,
    borderRadius: 10,
    width: 147,
    marginHorizontal: 131
  },
});