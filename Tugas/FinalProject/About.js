import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.image}>
            <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()} >
                <Icon name="menu" size={50} style={{width:35}} />
            </TouchableOpacity>
            <Image source={require('./images/about-image.png')} style={{ width:380, height: 347, alignItems: 'center' }} />
        </View>
        <View style={styles.logo}>
            <Image source={require('./images/logo-about.png')} style={{ width:203, height: 168, marginTop:5 }} />
        </View>
        <View style={styles.separator}>
            <Text style={{textAlign: 'center'}}>Ini adalah final project dari kelas Dasar React Native Sanbercode</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
  },
  image: {
    height: 397,
    marginHorizontal: 15,
    marginTop: 30
  },
  logo: {
    alignItems: 'center',
  },
  separator: {
    borderColor: '#000000',
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderRadius: 10,
    marginTop: 20,
    marginHorizontal: 50,
    alignItems: 'center'
  },
});